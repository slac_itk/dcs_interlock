#!/usr/bin/python3

import time
import math
import random
from datetime import datetime
from influxdb import InfluxDBClient
import smbus
import sys
import os
import subprocess
from smbus import SMBus
from sys import exit
from pprint import pprint


host = '192.168.1.1'
username = 'atlas'
password = 'AtlasItkDataBase'
database = 'lab'
port = 8086

bus = SMBus(1)

channels = {
    1: 0xB0,
    2: 0xB8,
    3: 0xB1,
    4: 0xB9,
    5: 0xB2,
    6: 0xBA,
    7: 0xB3,
    8: 0xBB,
    9: 0xB4,
    10: 0xBC,
    11: 0xB5,
    12: 0xBD,
    13: 0xB6,
    14: 0xBE,
    15: 0xB7,
    16: 0xBF
}
addresses = {
    "Interlock": 0b1110100,
    "Ring1": 0b1010110,
    "Ring2": 0b0010100
}

V_ref_Interlock = 3.3
V_ref_Ring = 3.2 #1.6
V_ref_Mon_scale_quad = (51 + 2000) / 51
V_ref_Mon_quad = V_ref_Ring * V_ref_Mon_scale_quad
V_ref_Mon_scale_trip = (3.3 + 2000) / 3.3 #3.3k ref resistance for Vmon triplet according to Andrew
V_ref_Mon_trip = V_ref_Ring * V_ref_Mon_scale_trip
R_ref = 3300 #Actually 3.3k ref resistance for NTCs according to Andrew, not 100kOhm Rref
lange = 0x06
zeit = 5
tiempo =  0.3
ADC_MAX = 8388608 # 2^23


def getReading(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    value = ((((reading[0]&0x3F))<<16)) + ((reading[1]<<8)) + (((reading[2]&0xE0)))
    return value

def convertNTC(volt):
    if volt == 0:
        return -999.
    # Values and equations taken from:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RD53AModuleTesting
    A = 0.8676453371787721e-3
    B = 2.541035850140508e-4
    C = 1.868520310774293e-7
    R_NTC = V_ref_Ring / volt * R_ref - R_ref
    T_NTC = 1 / (A + B * math.log(R_NTC) + C * (math.log(R_NTC))**3) - 273.15
    return T_NTC
    
time.sleep(tiempo)

#ring 1 (U2?) mapping
#1: P14 header?
#2: T1 Vmon?
#3: T1 NTC?
#4: T1 Vmon?
#5: T1 NTC?
#6: T1 Vmon?
#7: P6 Vmon
#8: P6 NTC
#9: P7 Vmon
#10: P7 NTC
#11: P8 Vmon
#12: P8 NTC
#13: P9 Vmon
#14: P9 NTC
#15: P10 Vmon
#16: P10 NTC (broken?)

#ring 2 (U3?) mapping
#1: P14 header?
#2: P14 header?
#3: T? Vmon?
#4: P14 header?
#5: T3 Vmon?
#6: T3 NTC?
#7: T2 Vmon? P1 Vmon? 
#8: T2 NTC?
#9: P2 Vmon
#10: P2 NTC
#11: P3 Vmon
#12: P3 NTC
#13: P4 Vmon
#14: P4 NTC
#15: P5 Vmon
#16: P5 NTC

# Q1 NTC goes to interlock
# Q3 NTC is presumably P10, but this seems broken
# Each entry is (i_ring, i_channel)
ntc_channels = {
    "Q1": (2, 8),
    "Q2": (2, 16),
    "Q4": (2, 12),
    "Q5": (2, 14),
    "Q6": (1, 8),
    "Q7": (2, 10),
    "Q8": (1, 10),
    "Q9": (1, 12),
    "Q10": (1, 14),
    "T1_chipB": (1, 3), # Triplet mapping is somewhat unsure, TBC
    "T1_chipC": (1, 5), # Triplet mapping is somewhat unsure, TBC
    "T2_chipA": (2, 6), # Triplet mapping is somewhat unsure, TBC
    # "T3_chipC": (2, 4) # Triplet mapping is somewhat unsure, TBC
}

vmon_channels = {
    "Q1": (2, 7),
    "Q2": (2, 15),
    "Q3": (1, 15),
    "Q4": (2, 11),
    "Q5": (2, 13),
    "Q6": (1, 7),
    "Q7": (2, 9),
    "Q8": (1, 9),
    "Q9": (1, 11),
    "Q10": (1, 13),
    "T1_chipA": (1, 6), # Triplet mapping is somewhat  unsure, TBC
    "T1_chipB": (1, 2), # Triplet mapping is somewhat  unsure, TBC
    "T1_chipC": (1, 4), # Triplet mapping is somewhat  unsure, TBC
    "T2_chipA": (2, 5),       # Triplet mapping is somewhat unsure, TBC
    "T3_chipC": (2, 3)        # Triplet mapping is somewhat unsure, TBC
}



if __name__ == '__main__':
    verbose = False
    vprint = pprint if verbose else lambda *x: None
    
    up_counter = 0

    while(True):
        data_ring = {}
        data_ring[f"emu_up_counter"] = up_counter

        try:
            # ###################################
            # # Interlock stuff
            # ###################################
            # data_interlock = {}
    
            # val1 = getReading(addresses["Interlock"], channels[1])
            # data_interlock["Int_channel1"] = V_ref_interlock * val1 / ADC_MAX
            # time.sleep(tiempo)
    
            # val2 = getReading(addresses["Interlock"], channels[2])
            # data_interlock["Int_channel2"] = V_ref_interlock * val2 / ADC_MAX
            # time.sleep(tiempo)

            ###################################
            # MOPS Emulator Ring
            ###################################

            # All the NTC Stuff
            for module_name, loc in ntc_channels.items():
                i_ring, i_ch = loc
                ring, ch = f"Ring{i_ring}", channels[i_ch]
                
                V_channel = float(V_ref_Ring * getReading(addresses[ring], ch) / ADC_MAX)
                data_ring[f"emu{i_ring}_ch{i_ch}"] = V_channel
                data_ring[f"emu{i_ring}_ch{i_ch}_NTC_{module_name}"] = convertNTC(V_channel)

                time.sleep(tiempo)

            # All the Vmon Stuff
            for module_name, loc in vmon_channels.items():
                i_ring, i_ch = loc
                ring, ch = f"Ring{i_ring}", channels[i_ch]
                
                V_channel = float(V_ref_Ring * getReading(addresses[ring], ch) / ADC_MAX)
                data_ring[f"emu{i_ring}_ch{i_ch}"] = V_channel

                if i_ring == 1 and i_ch == 6: # emu1_ch6 seems special---no voltage scaling necessary
                    data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{module_name}"] = V_channel
                elif module_name[0] == 'Q':
                    data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{module_name}"] = V_channel * V_ref_Mon_scale_quad
                elif module_name[0] == 'T':
                    data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{module_name}"] = V_channel * V_ref_Mon_scale_trip

                time.sleep(tiempo)
            
            # Vmon subtraction along the SP chain
            quad_SP_chain = ["Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q1", "Q2", "Q3", "Q4"]
            for i in range(len(quad_SP_chain) - 1):
                quad = quad_SP_chain[i+1]
                i_ring, i_ch = vmon_channels[quad]
                Vmon_after = data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]

                quad = quad_SP_chain[i]
                i_ring, i_ch = vmon_channels[quad]
                Vmon_before = data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]

                data_ring[f"emu{i_ring}_ch{i_ch}_DeltaV_{quad}"] = Vmon_before - Vmon_after

            # Deal with the last module in the SP chain separately
            quad = quad_SP_chain[len(quad_SP_chain) - 1]
            i_ring, i_ch = vmon_channels[quad]
            Vmon_before = data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]
            data_ring[f"emu{i_ring}_ch{i_ch}_DeltaV_{quad}"] = data_ring[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]

            # If everything was read-out and calculated successfully
            data_ring[f"emu_status"] = 0
            up_counter += 1

        except OSError as e:
            data_ring[f"emu_status"] = e.errno
            up_counter = 0
            
    
        

        vprint("--------------------------------------------------------------------------------")
        fields = {
        #     **data_interlock,
            **data_ring
        }
        vprint(fields)
        json_points = [{
             "measurement" : "on-detector-dcs",
             "tags" : {"location": "lab"},
             "time":datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
             "fields": fields
        }]

        try:
            db_client = InfluxDBClient(host, port, username, password, database)
            db_client.write_points(json_points)
    
            # Testing
            # print(db_client)
            # # pprint(db_client.ping())
            # pprint(db_client.get_list_database())
            # pprint(db_client.query("SELECT * FROM on-detector-dcs"))
        except Exception as e:
            print(e)
            
        time.sleep(tiempo)
