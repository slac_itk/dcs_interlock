import time
import math
from datetime import datetime
from influxdb import InfluxDBClient
import smbus
import sys
import os
import subprocess
from smbus import SMBus
from sys import exit
from pprint import pprint


host = '192.168.1.1'
username = 'atlas'
password = 'AtlasItkDataBase'
database = 'lab'
port = 8086



bus = SMBus(1)

channels = [0xB0,0xB8,0xB1,0xB9,0xB2,0xBA,0xB3,0xBB,0xB4,0xBC,0xB5,0xBD,0xB6,0xBE,0xB7,0xBF]
address_Interlock = 0b1110100
address_Ring1 = 0b1010110
address_Ring2 = 0b0010100

vmon_scale = (51 + 2000) / 51.
vref_Interlock = 3.3
vref_Ring = 3.2 #1.6
vref_Vmon = vref_Ring * vmon_scale
R_ref = 1e5 # 100kOhm R_ref
lange = 0x06
zeit = 5
tiempo =  0.3
max_reading = 8388608.0


def getReading_Interlock(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    value = ((((reading[0]&0x3F))<<16))+((reading[1]<<8))+(((reading[2]&0xE0)))
    volt = value * vref_Interlock/max_reading
    return volt

def getReading_Ring(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    value = ((((reading[0]&0x3F))<<16))+((reading[1]<<8))+(((reading[2]&0xE0)))
    volt = value * vref_Ring/max_reading
    return volt

def getReading_Vmon(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    #print(reading)
    value = ((((reading[0]&0x3F))<<16))+((reading[1]<<8))+(((reading[2]&0xE0)))
    volt = value * vref_Vmon/max_reading
    return volt

def convertReading_NTC(volt):
    # Values and equations taken from:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RD53AModuleTesting
    A = 0.8676453371787721e-3
    B = 2.541035850140508e-4
    C = 1.868520310774293e-7
    R_NTC = R_ref * volt / (vref_Ring - volt)
    # R_NTC = vref_Ring / volt * R_ref - R_ref
    T_NTC = 1 / (A + B * math.log(R_NTC) + C * (math.log(R_NTC))**3) - 273.15
    return T_NTC
    
time.sleep(tiempo)

ch_mult = 1

while(True):
    # Interlock_Ch0Value = ch_mult*getReading_Interlock(address_Interlock, channel0)

    # print("Channel 0 Reading: %12.2f V" % (Interlock_Ch0Value)) 
    # time.sleep(tiempo)
    # Interlock_Ch1Value = ch_mult*getReading_Interlock(address_Interlock, channel1)
    # print("Channel 1 Reading: %12.2f V" % (Interlock_Ch1Value)) 
    # time.sleep(tiempo)

    # i = 0
    # for channel_i in channel:
    #   i = i+1 
    #   # Ring1_ChValue = ch_mult*getReading_Ring(address_Ring1, channel_i)
    #   Ring1_ChValue = 100
    #   print("ring 1 channel" + str(i) + ":"+str(Ring1_ChValue))
    #   time.sleep(tiempo)
  
    i = 0
    
    for channel_i in channels:
        i = i+1

        if not ((i < 14 and i > 10) or i==12):
            continue
       
        Ring2_ChValue = ch_mult * getReading_Ring(address_Ring2, channel_i)
        Ring2_ChValue = 0.7
        # Even channels are NTC's
        if i % 2 == 0:
            Ring2_ChValue = convertReading_NTC(Ring2_ChValue)
        # Odd channels are LV's
        else:
            Ring2_ChValue *= vmon_scale
        print("ring 2 channel" + str(i) + ":"+str(Ring2_ChValue))
        time.sleep(tiempo)
     
    print("----")
    try:
        # json_body_interlock = [{
        #     "measurement" : "Interlock",
        #     "tags":{"location" : "lab"},
        #     "time":datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
        #     "fields":{ 'channel0': Interlock_Ch0Value,
        #                'channel1': Interlock_Ch1Value}
        #     }]
        # json_body_Ring1 = [{
        #     "measurement":"Ring Emulator 1",
        #     "tags":{"location" : "lab"},
        #     "time":datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
        #     "fields":{ 'channel0': Ring1_Ch0Value,
        #                'channel1': Ring1_Ch1Value,
        #                'channel2': Ring1_Ch2Value,
        #                'channel3': Ring1_Ch3Value,
        #                'channel4': Ring1_Ch4Value,
        #                'channel5': Ring1_Ch5Value,
        #                'channel6': Ring1_Ch6Value,
        #                'channel7': Ring1_Ch7Value,
        #                'channel8': Ring1_Ch8Value,
        #                'channel9': Ring1_Ch9Value,
        #                'channel10': Ring1_Ch10Value,
        #                'channel11': Ring1_Ch11Value,
        #                'channel12': Ring1_Ch12Value,
        #                'channel13': Ring1_Ch13Value,
        #                'channel14': Ring1_Ch14Value,
        #                'channel15': Ring1_Ch15Value,
        #                }
        #     }
        #     ]
        json_body_Ring2 = [{
            "measurement": "RingEmulator2",
            "time": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
            "fields":{ 'channel1': 100.,
                       'channel2': 100.,
                       'channel3': 100.,
                       'channel4': 100.,
                       'channel5': 100.,
                       'channel6': 100.,
                       'channel7': 100.,
                       'channel8': 100.,
                       'channel9': 100.,
                       'channel10': 100.,
                       'channel11': 300.,
                       'channel12': 300.,
                       'channel13': 300.,
                       'channel14': 300.,
                       'channel15': 300.,
                       'channel16': 300.}
            }]
        # json_body_Ring2 = [{
        #     "measurement":"Ring Emulator 2",
        #     "tags":{"location" : "lab"},
        #     "time":datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
        #     "fields":{ 'channel0': Ring2_Ch0Value,
        #                'channel1': Ring2_Ch1Value,
        #                'channel2': Ring2_Ch2Value,
        #                'channel3': Ring2_Ch3Value,
        #                'channel4': Ring2_Ch4Value,
        #                'channel5': Ring2_Ch5Value,
        #                'channel6': Ring2_Ch6Value,
        #                'channel7': Ring2_Ch7Value,
        #                'channel8': Ring2_Ch8Value,
        #                'channel9': Ring2_Ch9Value,
        #                'channel10': Ring2_Ch10Value,
        #                'channel11': Ring2_Ch11Value,
        #                'channel12': Ring2_Ch12Value,
        #                'channel13': Ring2_Ch13Value,
        #                'channel14': Ring2_Ch14Value,
        #                'channel15': Ring2_Ch15Value,}
        #     }]
        db_client = InfluxDBClient(host, port, username, password, database)
        # db_client.write_points(json_body_interlock)
        # db_client.write_points(json_body_Ring1)
        db_client.write_points(json_body_Ring2)

        # Testing
        # print(db_client)
        # # pprint(db_client.ping())
        # pprint(db_client.get_list_database())
        # print("Data from DB name 'lab'")
        # pprint(db_client.query("SELECT * FROM RingEmulator2"))
    except Exception as e: print(e)
    time.sleep(1)
    sys.stdout.flush
    
