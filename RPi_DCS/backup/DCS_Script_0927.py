import time
import math
import random
from datetime import datetime
from influxdb import InfluxDBClient
import smbus
import sys
import os
import subprocess
from smbus import SMBus
from sys import exit
from pprint import pprint


host = '192.168.1.1'
username = 'atlas'
password = 'AtlasItkDataBase'
database = 'lab'
port = 8086

bus = SMBus(1)

channels = {
    1: 0xB0,
    2: 0xB8,
    3: 0xB1,
    4: 0xB9,
    5: 0xB2,
    6: 0xBA,
    7: 0xB3,
    8: 0xBB,
    9: 0xB4,
    10: 0xBC,
    11: 0xB5,
    12: 0xBD,
    13: 0xB6,
    14: 0xBE,
    15: 0xB7,
    16: 0xBF
}
addresses = {
    "Interlock": 0b1110100,
    "Ring1": 0b1010110,
    "Ring2": 0b0010100
}

V_ref_Interlock = 3.3
V_ref_Ring = 3.2 #1.6
V_ref_Mon_scale_quad = (51 + 2000) / 51
V_ref_Mon_quad = V_ref_Ring * V_ref_Mon_scale_quad
V_ref_Mon_scale_trip = (3.3 + 2000) / 3.3 #3.3k ref resistance for Vmon triplet according to Andrew
V_ref_Mon_trip = V_ref_Ring * V_ref_Mon_scale_trip
R_ref = 3300 #Actually 3.3k ref resistance for NTCs according to Andrew, not 100kOhm Rref
lange = 0x06
zeit = 5
tiempo =  0.3
ADC_MAX = 8388608 # 2^23


def getReading(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    value = ((((reading[0]&0x3F))<<16)) + ((reading[1]<<8)) + (((reading[2]&0xE0)))
    return value

def convertNTC(volt):
    # Values and equations taken from:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RD53AModuleTesting
    A = 0.8676453371787721e-3
    B = 2.541035850140508e-4
    C = 1.868520310774293e-7
    R_NTC = V_ref_Ring / volt * R_ref - R_ref
    T_NTC = 1 / (A + B * math.log(R_NTC) + C * (math.log(R_NTC))**3) - 273.15
    return T_NTC
    
time.sleep(tiempo)

#ring 1 (U2?) mapping
#1: P14 header?
#2: T1 Vmon?
#3: T1 NTC?
#4: T1 Vmon?
#5: T1 NTC?
#6: T1 Vmon?
#7: P6 Vmon
#8: P6 NTC
#9: P7 Vmon
#10: P7 NTC
#11: P8 Vmon
#12: P8 NTC
#13: P9 Vmon
#14: P9 NTC
#15: P10 Vmon
#16: P10 NTC (broken?)

#ring 2 (U3?) mapping
#1: P14 header?
#2: P14 header?
#3: T? Vmon?
#4: P14 header?
#5: T3 Vmon?
#6: T3 NTC?
#7: T2 Vmon? P1 Vmon? 
#8: T2 NTC?
#9: P2 Vmon
#10: P2 NTC
#11: P3 Vmon
#12: P3 NTC
#13: P4 Vmon
#14: P4 NTC
#15: P5 Vmon
#16: P5 NTC

while(True):
    # ###################################
    # # Interlock stuff
    # ###################################
    # data_interlock = {}

    # val1 = getReading(addresses["Interlock"], channels[1])
    # data_interlock["Int_channel1"] = V_ref_interlock * val1 / ADC_MAX
    # time.sleep(tiempo)

    # val2 = getReading(addresses["Interlock"], channels[2])
    # data_interlock["Int_channel2"] = V_ref_interlock * val2 / ADC_MAX
    # time.sleep(tiempo)

    ###################################
    # Ring 1 stuff
    ###################################
    data_ring1 = {}
    for i, i_channel in channels.items():
        #if not (i in [3, 5, 8, 10, 12, 14]):
        #    continue
       
        V_channel = V_ref_Ring * getReading(addresses["Ring1"], i_channel) / ADC_MAX
        data = V_channel

        if (i in [3, 5] + [8, 10, 12, 14]): #triplet + quad NTC
            data = convertNTC(V_channel)
        if (i in [2, 4, 6]): #triplet voltage (speculation)
            data = V_channel * V_ref_Mon_scale_trip
        if (i in [7, 9, 11, 13]): #quad voltage
            data = V_channel * V_ref_Mon_scale_quad

        data_ring1[f"emulator1_channel{i}"] = float(data)
        print(f"Ring 1, channel{i}: {data}")
        time.sleep(tiempo)
    
    ###################################
    # Ring 2 stuff
    ###################################
    data_ring2 = {}
    for i, i_channel in channels.items():
        #if not (i in [6, 8, 10, 12, 14, 16]):
        #    continue

        V_channel = V_ref_Ring * getReading(addresses["Ring2"], i_channel) / ADC_MAX
        data = V_channel

        if (i in [6, 8] + [10, 12, 14, 16]): #triplet + quad NTC
            data = convertNTC(V_channel)
        if (i in [5, 7]): #triplet voltage (speculation)
            data = V_channel * V_ref_Mon_scale_trip
        if (i in [9, 11, 13, 15]): #quad voltage
            data = V_channel * V_ref_Mon_scale_quad

        data_ring2[f"emulator2_channel{i}"] = float(data)
        print(f"Ring 2, channel{i}: {data}")
        time.sleep(tiempo)

     
    print("----")
    fields = {
    #     **data_interlock,
        **data_ring1,
        **data_ring2
    }
    json_points = [{
         "measurement" : "on-detector-dcs",
         "tags" : {"location": "lab"},
         "time":datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
         "fields": fields
    }]
    try:
        db_client = InfluxDBClient(host, port, username, password, database)
        db_client.write_points(json_points)

        # Testing
        # print(db_client)
        # # pprint(db_client.ping())
        pprint(db_client.get_list_database())
        pprint(db_client.query("SELECT * FROM RingEmulator2"))
    except Exception as e:
        print(e)
        
    #time.sleep(1)
    # sys.stdout.flush
    
