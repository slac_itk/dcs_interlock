#!/usr/bin/python3

import time
import RPi.GPIO as GPIO
# from threading import Thread, Event
import sys
import os
import psutil
import signal
from pprint import pprint
from functools import reduce



def main():
    verbose = True
    vprint = pprint if verbose else lambda *x: None

    duration = 0.1
    tiempo = 1 - duration
    beat_pin = 32
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(beat_pin, GPIO.OUT)
    GPIO.output(beat_pin, 0)

    def pulse(duration):
        GPIO.output(beat_pin, 1)
        time.sleep(duration)
        GPIO.output(beat_pin, 0)

    def check_process(pname):
        try:
            for p in processes:
                cmd_args = [c.split('/')[-1] for c in p.cmdline()]
                if "python3" in cmd_args and pname in cmd_args:
                    return True
        except psutil.NoSuchProcess as e:
            print(e)

        return False

    check_list = [
        "Interlock_Script.py",
        "DCS_Script.py"
    ]

    while True:
        processes = [p for p in psutil.process_iter()]

        beat = reduce(lambda a, b: a and b, [check_process(x) for x in check_list])
        #beat = True

        if beat:
            vprint("Beat! All processes in check_list found!")
            pulse(duration)
        else:
            vprint("No beat! Some processes not found!")
            time.sleep(duration)

        time.sleep(tiempo)

    # ##Note Time_Delay is how many seconds the pulse will be.
    # def Pulse(Time_Delay):
    #         while(Time_Delay['running']):
    #                 GPIO.output(Output_Pin, 1)
    #                 sleep(Time_Delay['delay'])
    #                 GPIO.output(Output_Pin, 0)
    #                 sleep(Time_Delay['delay'])
    # 
    # pulse_delay = dict()
    # pulse_delay["running"] = 1
    # pulse_delay["delay"] = .1
    # thread_HB = Thread(target=Pulse, args=(pulse_delay, ))
    # thread_HB.start()

def find_duplicate(pname):
    processes = [p for p in psutil.process_iter()]

    dup = []
    for p in processes:
        cmd_args = [c.split('/')[-1] for c in p.cmdline()]
        if "python3" in cmd_args and pname in cmd_args and p.pid != os.getpid():
            dup.append(p.pid)

    return dup


if __name__ == '__main__':
    pname = sys.argv[0].split('/')[-1]

    duplicate = find_duplicate(pname)

    if not duplicate:
        main()
    else:
        if len(sys.argv) > 1:
            if sys.argv[1] == "restart":
                for pid in duplicate:
                    os.kill(pid, signal.SIGKILL)
                main()
            else:
                print(f"Unknown argument `{sys.argv[1]}` provided!")
        else:
            print("Process already running!")
