from influxdb import InfluxDBClient
import time
from smbus import SMBus
bus = SMBus(1)

db_params = {
    "host": "192.168.1.1",
    "username": "atlas",
    "password": "AtlasItkDataBase",
    "database": "lab",
    "port": 8086,
}
db_client = InfluxDBClient(
    db_params["host"],
    db_params["port"],
    db_params["username"],
    db_params["password"],
    db_params["database"]
)

tiempo = 0.3

binary_channels = {
    1: 0xB0,
    2: 0xB8,
    3: 0xB1,
    4: 0xB9,
    5: 0xB2,
    6: 0xBA,
    7: 0xB3,
    8: 0xBB,
    9: 0xB4,
    10: 0xBC,
    11: 0xB5,
    12: 0xBD,
    13: 0xB6,
    14: 0xBE,
    15: 0xB7,
    16: 0xBF
}

lange = 0x06

board_addresses = {
    # "Interlock": 0b1110100,        # "Old" address
    "Interlock": 0b0100110,        # Update: 20221206
    "Ring1": 0b1010110,
    "Ring2": 0b0010100
}

ADC_MAX = 8388608 # 2^23

#ring 1 (U2?) mapping
#1: P14 header?
#2: T1 Vmon?
#3: T1 NTC?
#4: T1 Vmon?
#5: T1 NTC?
#6: T1 Vmon?
#7: P6 Vmon
#8: P6 NTC
#9: P7 Vmon
#10: P7 NTC
#11: P8 Vmon
#12: P8 NTC
#13: P9 Vmon
#14: P9 NTC
#15: P10 Vmon
#16: P10 NTC (broken?)

#ring 2 (U3?) mapping
#1: P14 header?
#2: P14 header?
#3: T? Vmon?
#4: P14 header?
#5: T3 Vmon?
#6: T3 NTC?
#7: T2 Vmon? P1 Vmon? 
#8: T2 NTC?
#9: P2 Vmon
#10: P2 NTC
#11: P3 Vmon
#12: P3 NTC
#13: P4 Vmon
#14: P4 NTC
#15: P5 Vmon
#16: P5 NTC
# Each entry is (i_ring, i_channel)

emu_ntc_channels = {
    "Q1": (2, 8),
    "Q2": (2, 16),
    "Q4": (2, 12),
    "Q5": (2, 14),
    "Q6": (1, 8),
    "Q7": (2, 10),
    "Q8": (1, 10),
    "Q9": (1, 12),
    "Q10": (1, 14),
    "T1_chipB": (1, 3), # Triplet mapping is somewhat unsure, TBC
    "T1_chipC": (1, 5), # Triplet mapping is somewhat unsure, TBC
    "T2_chipA": (2, 6), # Triplet mapping is somewhat unsure, TBC
    # "T3_chipC": (2, 4) # Triplet mapping is somewhat unsure, TBC
}

emu_vmon_channels = {
    "Q1": (2, 7),
    "Q2": (2, 15),
    "Q3": (1, 15),
    "Q4": (2, 11),
    "Q5": (2, 13),
    "Q6": (1, 7),
    "Q7": (2, 9),
    "Q8": (1, 9),
    "Q9": (1, 11),
    "Q10": (1, 13),
    "T1_chipA": (1, 6), # Triplet mapping is somewhat  unsure, TBC
    "T1_chipB": (1, 2), # Triplet mapping is somewhat  unsure, TBC
    "T1_chipC": (1, 4), # Triplet mapping is somewhat  unsure, TBC
    "T2_chipA": (2, 5),       # Triplet mapping is somewhat unsure, TBC
    "T3_chipC": (2, 3)        # Triplet mapping is somewhat unsure, TBC
}




def getReading(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    value = ((((reading[0]&0x3F))<<16)) + ((reading[1]<<8)) + (((reading[2]&0xE0)))
    return value
