import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

##############################################
# LV PS Stuff
##############################################
PS_CONTROL_PIN = 37

GPIO.setup(PS_CONTROL_PIN, GPIO.OUT)
GPIO.output(PS_CONTROL_PIN, 0)
def TURN_PS_OFF():
    print("Turning off PS!")
    GPIO.output(PS_CONTROL_PIN, 1)

##############################################
# TODO: CO2 Pump Stuff
##############################################
