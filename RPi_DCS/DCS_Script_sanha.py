#!/usr/bin/python3

import time
import math
import random
from datetime import datetime
from influxdb import InfluxDBClient
import sys
import os
import subprocess
import RPi.GPIO as GPIO
from sys import exit
from pprint import pprint

import digital
import analog

if __name__ == '__main__':
    verbose = True
    vprint = pprint if verbose else lambda *x: None
    
    t0 = datetime.utcnow()
    up_counter = 0

    while(True):
        time.sleep(digital.tiempo)

        data_dict = {}

        try:
            ###################################
            # MOPS Emulator Ring
            ###################################

            # All the NTC Stuff
            for module_name, loc in digital.emu_ntc_channels.items():
                i_ring, i_ch = loc
                ring, ch = f"Ring{i_ring}", digital.binary_channels[i_ch]
                address = digital.board_addresses[ring]
                
                V_channel = float(analog.V_ref_Ring * digital.getReading(address, ch) / digital.ADC_MAX)
                data_dict[f"emu{i_ring}_ch{i_ch}"] = V_channel

                # if V_channel = 0, NTC conversion is impossible; simply don't record anything
                if V_channel > 1e-3:
                    T = analog.convertNTC(V_channel, analog.R_ref_Ring)
                    data_dict[f"emu{i_ring}_ch{i_ch}_NTC_{module_name}"] = T

                time.sleep(digital.tiempo)

            # All the Vmon Stuff
            for module_name, loc in digital.emu_vmon_channels.items():
                i_ring, i_ch = loc
                ring, ch = f"Ring{i_ring}", digital.binary_channels[i_ch]
                address = digital.board_addresses[ring]
                
                V_channel = float(analog.V_ref_Ring * digital.getReading(address, ch) / digital.ADC_MAX)
                data_dict[f"emu{i_ring}_ch{i_ch}"] = V_channel

                if i_ring == 1 and i_ch == 6: # emu1_ch6 seems special---no voltage scaling necessary
                    data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{module_name}"] = V_channel
                elif module_name[0] == 'Q':
                    data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{module_name}"] = V_channel * analog.V_ref_Mon_scale_quad
                elif module_name[0] == 'T':
                    data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{module_name}"] = V_channel * analog.V_ref_Mon_scale_trip

                time.sleep(digital.tiempo)
            
            # Vmon subtraction along the SP chain
            quad_SP_chain = ["Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q1", "Q2", "Q3", "Q4"]
            for i in range(len(quad_SP_chain) - 1):
                quad = quad_SP_chain[i+1]
                i_ring, i_ch = digital.emu_vmon_channels[quad]
                Vmon_after = data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]

                quad = quad_SP_chain[i]
                i_ring, i_ch = digital.emu_vmon_channels[quad]
                Vmon_before = data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]

                data_dict[f"emu{i_ring}_ch{i_ch}_DeltaV_{quad}"] = Vmon_before - Vmon_after

            # Deal with the last module in the SP chain separately
            quad = quad_SP_chain[len(quad_SP_chain) - 1]
            i_ring, i_ch = digital.emu_vmon_channels[quad]
            Vmon_before = data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]
            data_dict[f"emu{i_ring}_ch{i_ch}_DeltaV_{quad}"] = data_dict[f"emu{i_ring}_ch{i_ch}_Vmon_{quad}"]

            # If everything was read-out and calculated successfully
            data_dict[f"emu_status"] = 0
            up_counter += 1
            # data_dict[f"emu_up_counter"] = up_counter

            t = datetime.utcnow()
            data_dict[f"emu_up_time"] = (t - t0).total_seconds()

        except OSError as e:
            data_dict[f"emu_status"] = e.errno
            up_counter = 0     
            # data_dict[f"emu_up_counter"] = up_counter
            t0 = datetime.now()
            data_dict[f"emu_up_time"] = 0.

        vprint("--------------------------------------------------------------------------------")
        fields = {
            **data_dict
        }
        vprint(fields)
        json_points = [{
             "measurement" : "on-detector-dcs",
             "tags" : {"location": "lab"},
             "time":datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
             "fields": fields
        }]

        try:
            db_client = InfluxDBClient(
                digital.DB["host"],
                digital.DB["port"],
                digital.DB["username"],
                digital.DB["password"],
                digital.DB["database"]
            )
            db_client.write_points(json_points)
    
            # Testing
            # print(db_client)
            # # pprint(db_client.ping())
            # pprint(db_client.get_list_database())
            # pprint(db_client.query("SELECT * FROM on-detector-dcs"))
        except Exception as e:
            print(e)
            
        time.sleep(digital.tiempo)
