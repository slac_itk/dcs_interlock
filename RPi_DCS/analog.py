import math

# V_ref_Interlock = 3.3
V_ref_Ring = 3.2 #1.6
V_ref_Mon_scale_quad = (51 + 2000) / 51
V_ref_Mon_quad = V_ref_Ring * V_ref_Mon_scale_quad
# 2022/09/28 used this value
# V_ref_Mon_scale_trip = (3.3 + 2000) / 3.3 #3.3k ref resistance for Vmon triplet according to Andrew
# Fixed after 2022/09/28
V_ref_Mon_scale_trip = (3.3 + 10) / 3.3 #3.3k ref resistance for Vmon triplet according to Andrew
V_ref_Mon_trip = V_ref_Ring * V_ref_Mon_scale_trip
R_ref_Ring = 3300 # Actually 3.3k ref resistance for NTCs according to Andrew, not 100kOhm Rref
R_ref_Interlock = {
    1: 10000,    # Nominal R_ref for the interlock NTC as per Fengrui's design (2022-10-24)
    2: 10000     # R_ref value tuned/calibrated manually at room T by Sanha (2022-10-24)
}

def convertNTC(volt, R_ref):
    # if volt == 0:
    #     return -999.

    # Values and equations taken from:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RD53AModuleTesting
    A = 0.8676453371787721e-3
    B = 2.541035850140508e-4
    C = 1.868520310774293e-7
    R_NTC = V_ref_Ring / volt * R_ref - R_ref
    T_NTC = 1 / (A + B * math.log(R_NTC) + C * (math.log(R_NTC))**3) - 273.15
    return T_NTC
