#!/usr/bin/python3

import time
import math
import random
from datetime import datetime
from influxdb import InfluxDBClient
import sys
import os
import psutil
import signal
import serial
import RPi.GPIO as GPIO
from sys import exit
from pprint import pprint

import digital
import analog
import control

    

interlock_ntc_channels = {
    "Q3": 1,
    # "T1_chipA": 2, # Read-out unstable (getting flat zeroes or very small values), turned off temporarily (Sanha, 2022-10-24)
}

def main():
    verbose = True
    vprint = pprint if verbose else lambda *x: None
    upload_to_db = True

    tiempo = 0.3

    t0 = datetime.utcnow()

    NTC_THRESHOLD = 35             # turn PS off if T_NTC > 35 degC

    dmt_serial = serial.Serial("/dev/ttyUSB0", 19200, timeout=1)
    dmt_serial.write(b'form 3.2 Tdf "," 3.2 Tdfa "," 6.0 H2O "," STAT ":"#r#n \r\n')
    dmt_serial.write(b'r\r\n')
    dmt_status_code = {
        'h': 1,
        'H': 2,
        'A': 3
    }

    while(True):
        time.sleep(tiempo)

        ###################################
        # Interlock stuff
        ###################################
        interlock_data_dict = {}
        try:
            interlock_address = digital.board_addresses["Interlock"]
            for module_name, i_ch in interlock_ntc_channels.items():
                ch = digital.binary_channels[i_ch]
                V_channel = float(analog.V_ref_Ring * digital.getReading(interlock_address, ch) / digital.ADC_MAX)
                interlock_data_dict[f"interlock_ch{i_ch}"] = V_channel
                # if V_channel = 0, NTC conversion is impossible; simply don't record anything
                if V_channel > 1e-2:
                    T = analog.convertNTC(V_channel, analog.R_ref_Interlock[i_ch])
                    interlock_data_dict[f"interlock_ch{i_ch}_NTC_{module_name}"] = T
                    if T > NTC_THRESHOLD:
                        control.TURN_PS_OFF()

                time.sleep(tiempo)

            # If everything was read-out and calculated successfully
            interlock_data_dict[f"interlock_status"] = 0
            t = datetime.utcnow()
            interlock_data_dict[f"interlock_up_time"] = (t - t0).total_seconds()


        except OSError as e:
            print(e)
            interlock_data_dict[f"interlock_status"] = e.errno
            t0 = datetime.utcnow()
            interlock_data_dict[f"interlock_up_time"] = 0.

        ###################################
        # DMT stuff
        ###################################
        dmt_data_dict = {}
        try:
            line = str(dmt_serial.readline())
            # vprint(line)
            line = line.replace(' ', '').rstrip('\n').rstrip('\r')
            line = line.split(',')
            # vprint(line)

            Tdf = float(line[0].replace("b'", ''))
            Tdfatm = float(line[1])
            H2O = float(line[2])
            status = line[3].split(':')[0]
            status = dmt_status_code.get(status, 0)

            dmt_data_dict = {
                "Tdf" : Tdf,
                "Tdfatm" : Tdfatm,
                "H2O" : H2O,
                "status" : status
            }


        except Exception as e:
            print(e)

        vprint("--------------------------------------------------------------------------------")
        write_time = datetime.utcnow()
        vprint({**interlock_data_dict})
        interlock_json = [{
             "measurement": "on-detector-dcs",
             "tags": {"location": "lab"},
             "time": write_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
             "fields": {**interlock_data_dict}
        }]
        if upload_to_db:
            try:
                digital.db_client.write_points(interlock_json)
            except Exception as e:
                print(e)

        vprint({**dmt_data_dict})
        dmt_json = [{
            "measurement": "dmt143",
            "tags" : {"location": "lab"},
            "time" : write_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
            "fields": {**dmt_data_dict}
        }]
        if upload_to_db:
            try:
                digital.db_client.write_points(dmt_json)
            except Exception as e:
                print(e)

def find_duplicate(pname):
    processes = [p for p in psutil.process_iter()]

    dup = []
    for p in processes:
        cmd_args = [c.split('/')[-1] for c in p.cmdline()]
        if "python3" in cmd_args and pname in cmd_args and p.pid != os.getpid():
            dup.append(p.pid)

    return dup


if __name__ == '__main__':
    pname = sys.argv[0].split('/')[-1]

    duplicate = find_duplicate(pname)

    if not duplicate:
        main()
    else:
        if len(sys.argv) > 1:
            if sys.argv[1] == "restart":
                for pid in duplicate:
                    os.kill(pid, signal.SIGKILL)
                main()
            else:
                print(f"Unknown argument `{sys.argv[1]}` provided!")
        else:
            print("Process already running!")
