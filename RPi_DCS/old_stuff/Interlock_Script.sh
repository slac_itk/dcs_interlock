#!/bin/bash

DIR="/home/pi/DCS_Interlock"

Interlock_pid=$(pgrep -f Interlock_Script.py)
if [ -z $Interlock_pid ]; then
    cd $DIR
    ./Interlock_Script.py
    exit
else
    if [ "$1" == "restart" ]; then
        kill $Interlock_pid
        sleep 1
        cd $DIR
        ./Interlock_Script.py
        exit
    fi
fi
