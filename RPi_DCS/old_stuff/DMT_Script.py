#r#n #!/usr/bin/python3

import time
import math
import random
from datetime import datetime
from influxdb import InfluxDBClient
import serial
import sys
import os
import psutil
import signal
import re
import time

import digital


def main():
    tiempo = 0.5

    ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=1)
    ser.write(b'form 3.2 Tdf "," 3.2 Tdfa "," 6.0 H2O "," STAT ":"#r#n \r\n')
    ser.write(b'r\r\n')
    
    while(True):
        try:
            line=str(ser.readline())
    #        print(line, flush=True)
            line=line.replace(' ','')
            line=line.rstrip('\n').rstrip('\r')
            line=line.split(',')
    
            t1=float(line[0].replace("b'",''))
            t2=float(line[1])
            ppm=float(line[2])
            stat=line[3].split(':')[0]
            status=0
            if(stat == 'h'):
                status = 1
            if(stat == 'H'):
                status = 2
            if(stat == 'A'):
                status = 3
    
            json_body = [{
                "measurement": "dmt143",
                "tags" : {"location": "lab"},
                "time" : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": { 'Tdf' : t1, "Tdfatm" : t2, 'H2O' : ppm, 'status' : status}
            }]
    
            digital.db_client.write_points(json_body)
        except Exception as e:
            print(e)
    
        time.sleep(1)

def find_duplicate(pname):
    processes = [p for p in psutil.process_iter()]

    dup = []
    for p in processes:
        cmd_args = [c.split('/')[-1] for c in p.cmdline()]
        if "python3" in cmd_args and pname in cmd_args and p.pid != os.getpid():
            dup.append(p.pid)

    return dup


if __name__ == '__main__':
    pname = sys.argv[0].split('/')[-1]

    duplicate = find_duplicate(pname)

    if not duplicate:
        main()
    else:
        if len(sys.argv) > 1:
            if sys.argv[1] == "restart":
                for pid in duplicate:
                    os.kill(pid, signal.SIGKILL)
                main()
            else:
                print(f"Unknown argument `{sys.argv[1]}` provided!")
        else:
            print("Process already running!")
