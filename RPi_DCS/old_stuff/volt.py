import time
import math
import random
from datetime import datetime
from influxdb import InfluxDBClient
import smbus
import sys
import os
import subprocess
from smbus import SMBus
from sys import exit
from pprint import pprint
V_ref_Interlock = 3.3
V_ref_Ring = 5 #1.6
V_ref_Mon_scale_quad = (51 + 2000) / 51
V_ref_Mon_quad = V_ref_Ring * V_ref_Mon_scale_quad
V_ref_Mon_scale_trip = (3.3 + 2000) / 3.3 #3.3k ref resistance for Vmon triplet according to Andrew
V_ref_Mon_trip = V_ref_Ring * V_ref_Mon_scale_trip
R_ref = 3300 #Actually 3.3k ref resistance for NTCs according to Andrew, not 100kOhm Rref
lange = 0x06
zeit = 5
tiempo =  0.3
ADC_MAX = 8388608 # 2^23
def convertNTC(volt):
    # Values and equations taken from:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RD53AModuleTesting
    A = 0.8676453371787721e-3
    B = 2.541035850140508e-4
    C = 1.868520310774293e-7
    R_NTC = V_ref_Ring / volt * R_ref - R_ref
    print(R_NTC)
    T_NTC = 1 / (A + B * math.log(R_NTC) + C * (math.log(R_NTC))**3) - 273.15
    print (T_NTC)
    return T_NTC

convertNTC(0.08619916579771)
