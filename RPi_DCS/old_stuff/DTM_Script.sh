#!/bin/bash

DIR="/home/pi/DCS_Interlock"

DTM_pid=$(pgrep -f DTM_Script.py)
if [ -z $DTM_pid ]; then
    cd $DIR
    ./DTM_Script.py
    exit
else
    if [ "$1" == "restart" ]; then
        kill $DTM_pid
        sleep 1
        cd $DIR
        ./DTM_Script.py
        exit
    fi
fi
