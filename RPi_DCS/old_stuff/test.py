import math
R_ref = 100000
vref_Ring = 3.2
def convertReading_NTC(volt):
    # Values and equations taken from:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RD53AModuleTesting
    A = 0.8676453371787721e-3
    B = 2.541035850140508e-4
    C = 1.868520310774293e-7
    #R_NTC = R_ref * volt / (vref_Ring - volt)
    R_NTC = R_ref * vref_Ring/volt - R_ref
    T_NTC = 1 / (A + B * math.log(R_NTC) + C * (math.log(R_NTC))**3) - 273.15
    return T_NTC

print(convertReading_NTC(0.710))
