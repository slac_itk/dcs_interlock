#!/bin/bash

DIR="/home/pi/DCS_Interlock"

DCS_pid=$(pgrep -f DCS_Script.py)
if [ -z $DCS_pid ]; then
    cd $DIR
    ./DCS_Script.py
    exit
else
    if [ "$1" == "restart" ]; then
        kill $DCS_pid
        sleep 1
        cd $DIR
        ./DCS_Script.py
        exit
    fi
fi
