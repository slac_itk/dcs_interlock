#!/bin/bash

DIR="/home/pi/DCS_Interlock"

pid=$(pgrep -f HeartBeat.py)
if [ -z $pid ]; then
    cd $DIR
    ./HeartBeat.py
    exit
else
    if [ "$1" == "restart" ]; then
        kill $pid
        sleep 1
        cd $DIR
        ./HeartBeat.py
        exit
    fi
fi
